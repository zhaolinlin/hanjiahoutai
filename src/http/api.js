import {http} from './index';


//用户登录
export function login(data){
    return http('login','POST',data);
}
//获取用户权限列表
export function getMenus(){
    return http('menus','GET');
}
//获取用户
export function getUsers(params){
    return http('users','GET',{},params)
}