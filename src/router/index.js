import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    { path: '/', redirect: '/Login' },
    {
        path: '/Login',
        name: 'Login',
        component: () => import(/* webpackChunkName: "Login" */ '../views/Login.vue')
    },
    {
        path: '/index',
        name: 'index',
        component: () => import(/* webpackChunkName: "Index" */ '../views/Index.vue'),
        children:[
            { path: '/index/', redirect: 'users' },
            {
                path:'users',
                name:'users',
                component: () => import(/* webpackChunkName: "users" */ '../components/users/Users.vue'),
            },
            {
                path:'roles',
                name:'roles',
                component: () => import(/* webpackChunkName: "roles" */ '../components/roles/Roles.vue'),
            },
            {
                path:'goods',
                name:'goods',
                component: () => import(/* webpackChunkName: "goods" */ '../components/goods/Goods.vue'),
            },
            {
                path:'orders',
                name:'orders',
                component: () => import(/* webpackChunkName: "orders" */ '../components/orders/Orders.vue'),
            },
            {
                path:'categories',
                name:'categories',
                component: () => import(/* webpackChunkName: "categories" */ '../components/categories/Categories.vue'),
            },
            {
                path:'params',
                name:'params',
                component: () => import(/* webpackChunkName: "params" */ '../components/params/Params.vue'),
            },
            {
                path:'reports',
                name:'reports',
                component: () => import(/* webpackChunkName: "reports" */ '../components/reports/Reports.vue'),
            },
            {
                path:'rights',
                name:'rights',
                component: () => import(/* webpackChunkName: "rights" */ '../components/rights/Rights.vue'),
            }
        ]
    }
]

const router = new VueRouter({
    routes
})

export default router
